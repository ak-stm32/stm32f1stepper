/*
 * AK_PWM.h
 *
 *  Created on: 30 ���. 2016 �.
 *      Author: pervoliner
 */

#ifndef AK_PWM_H_
#define AK_PWM_H_

#include "stm32f1xx.h"
#include "stm32f1xx_hal.h"


const struct _AK_PWM {
	void (*Init)(void);
	void (*TIM_PWM_MspInit)(void);
	void (*MspPostInit)(void);
} _AK_PWM;


#endif /* AK_FYQ5641_H_ */
