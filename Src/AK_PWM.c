/*
 * AK_PWM.c
 */
#include "AK_PWM.h"

TIM_HandleTypeDef _htim2;

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);
void _AK_PWM_TIM_Init(void);
void _AK_PWM_GPIO_Init(void);

extern void Error_Handler(void);

void _AK_PWM_Init(void);
void _AK_PWM_GPIO_Init(void);

void _AK_PWM_Init(void) {
	_AK_PWM_TIM_Init();

	HAL_TIM_PWM_Start(&_htim2, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&_htim2, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&_htim2, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&_htim2, TIM_CHANNEL_4);
}

void _AK_PWM_TIM_Init(void) {
	  TIM_MasterConfigTypeDef sMasterConfig;
	  TIM_OC_InitTypeDef sConfigOC;

	  _htim2.Instance = TIM2;
	  _htim2.Init.Prescaler = 45000;
	  _htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
	  _htim2.Init.Period = 100;
	  _htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
	  if (HAL_TIM_PWM_Init(&_htim2) != HAL_OK)
	  {
	    Error_Handler();
	  }

	  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
	  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
	  if (HAL_TIMEx_MasterConfigSynchronization(&_htim2, &sMasterConfig) != HAL_OK)
	  {
	    Error_Handler();
	  }

	  sConfigOC.OCMode = TIM_OCMODE_PWM1;
	  sConfigOC.Pulse = 20;
	  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	  if (HAL_TIM_PWM_ConfigChannel(&_htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
	  {
	    Error_Handler();
	  }

	  sConfigOC.Pulse = 40;
	  sConfigOC.OCPolarity = TIM_OCPOLARITY_LOW;
	  if (HAL_TIM_PWM_ConfigChannel(&_htim2, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  sConfigOC.Pulse = 60;
	  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
	  if (HAL_TIM_PWM_ConfigChannel(&_htim2, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
	  {
	    Error_Handler();
	  }
	  sConfigOC.Pulse = 80;
	  sConfigOC.OCPolarity = TIM_OCPOLARITY_LOW;
	  if (HAL_TIM_PWM_ConfigChannel(&_htim2, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
	  {
	    Error_Handler();
	  }

	  HAL_TIM_MspPostInit(&_htim2);
}

void _AK_PWM_MspPostInit(void) {
	_AK_PWM_GPIO_Init();
}

void _AK_PWM_MspInit(void) {
	__HAL_RCC_TIM2_CLK_ENABLE();
}


void _AK_PWM_GPIO_Init(void) {
	__GPIOA_CLK_ENABLE();
	__GPIOB_CLK_ENABLE();


	GPIO_InitTypeDef outDef;

	outDef.Pin = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3;
	outDef.Mode = GPIO_MODE_AF_PP;
	outDef.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOA, &outDef);

}

const struct _AK_PWM AK_PWM = {
    .Init = _AK_PWM_Init,
	.TIM_PWM_MspInit = _AK_PWM_MspInit,
	.MspPostInit = _AK_PWM_MspPostInit
};
